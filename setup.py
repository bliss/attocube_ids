from setuptools import setup, find_packages

setup(
    name="attocube_ids",
    version="1.7",
    packages=find_packages(),
)

